package com.example.a20220823_jaydeepshetye_nycschools.utils

import com.example.a20220823_jaydeepshetye_nycschools.model.School

sealed class ResponseState {
    data class Success(val data: List<School>) : ResponseState()
    data class Failure(val exception: Exception) : ResponseState()
    object Loading : ResponseState()
}

