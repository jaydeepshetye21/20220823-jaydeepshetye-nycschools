package com.example.a20220823_jaydeepshetye_nycschools.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class School(
    val school_name: String?,
    val phone_number: String?,
    val overview_paragraph: String?,
    val language_classes: String?,
    val school_email: String?
): Parcelable

