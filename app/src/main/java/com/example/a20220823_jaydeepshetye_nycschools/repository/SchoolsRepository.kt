package com.example.a20220823_jaydeepshetye_nycschools.repository

import com.example.a20220823_jaydeepshetye_nycschools.api.SchoolsAPI
import com.example.a20220823_jaydeepshetye_nycschools.model.School
import com.example.a20220823_jaydeepshetye_nycschools.utils.ResponseState
import com.example.a20220823_jaydeepshetye_nycschools.utils.ResponseState.Failure
import com.example.a20220823_jaydeepshetye_nycschools.utils.ResponseState.Success
import javax.inject.Inject

class SchoolsRepository @Inject constructor(
    private val schoolsAPI: SchoolsAPI
) {

    suspend fun getSchoolsList(): ResponseState {
        return try {
            val response = schoolsAPI.getSchoolsData()

            val body = response.body()
            if (response.isSuccessful && body != null) {
                Success(body)
            } else {
                Failure(Exception())
            }
        } catch (e: Exception) {
            Failure(e)
        }
    }
}

