package com.example.a20220823_jaydeepshetye_nycschools.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.a20220823_jaydeepshetye_nycschools.model.School
import com.example.a20220823_jaydeepshetye_nycschools.repository.SchoolsRepository
import com.example.a20220823_jaydeepshetye_nycschools.utils.ResponseState
import com.example.a20220823_jaydeepshetye_nycschools.utils.ResponseState.Loading
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SchoolsViewModel @Inject constructor(
    private val repository: SchoolsRepository
) : ViewModel() {

    private val _schools: MutableLiveData<ResponseState> = MutableLiveData()
    val schools: LiveData<ResponseState> = _schools

    init {
        getSchoolsData()
    }

    private fun getSchoolsData(){
        viewModelScope.launch {
            _schools.value = Loading
            val response = repository.getSchoolsList()
            _schools.value = response
        }
    }
}