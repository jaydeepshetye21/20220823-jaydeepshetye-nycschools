package com.example.a20220823_jaydeepshetye_nycschools.ui

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.a20220823_jaydeepshetye_nycschools.R
import com.example.a20220823_jaydeepshetye_nycschools.databinding.FragmentSchoolsListBinding
import com.example.a20220823_jaydeepshetye_nycschools.model.School
import com.example.a20220823_jaydeepshetye_nycschools.ui.adapter.SchoolAdapter
import com.example.a20220823_jaydeepshetye_nycschools.utils.ResponseState.*
import com.example.a20220823_jaydeepshetye_nycschools.viewmodel.SchoolsViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SchoolsListFragment : Fragment(R.layout.fragment_schools_list) {

    private val viewModel: SchoolsViewModel by viewModels()
    private lateinit var binding: FragmentSchoolsListBinding
    private lateinit var schoolAdapter: SchoolAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentSchoolsListBinding.bind(view)

        schoolAdapter = SchoolAdapter { school ->
            navigate(school)
        }

        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(requireContext())
            setHasFixedSize(true)
            adapter = schoolAdapter
        }

        viewModel.schools.observe(viewLifecycleOwner) { state ->
            when (state) {
                is Success -> {
                    displayProgressBar(false)
                    schoolAdapter.setData(state.data)
                }
                is Failure -> {
                    displayProgressBar(false)
                    displayError(state.exception.message)
                }
                is Loading -> {
                    displayProgressBar(true)
                }
            }
        }
    }

    private fun displayProgressBar(shouldDisplay: Boolean) {
        binding.progressBar.visibility = if (shouldDisplay) View.VISIBLE else View.GONE
    }

    private fun navigate(school: School) {
        val action =
            SchoolsListFragmentDirections.actionSchoolsListFragmentToSchoolsDetailFragment(school)
        findNavController().navigate(action)
    }

    private fun displayError(message: String?) {
        if (!message.isNullOrEmpty()) {
            Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(requireContext(), "Unknown Error", Toast.LENGTH_SHORT).show()
        }
    }
}

