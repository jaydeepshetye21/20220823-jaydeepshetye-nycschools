package com.example.a20220823_jaydeepshetye_nycschools.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.a20220823_jaydeepshetye_nycschools.databinding.ListItemBinding
import com.example.a20220823_jaydeepshetye_nycschools.model.School

class SchoolAdapter(
    private var itemClicked: ((school: School) -> Unit)
): RecyclerView.Adapter<SchoolAdapter.SchoolViewHolder>() {

    private var schoolList = emptyList<School>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolViewHolder {
        val binding = ListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SchoolViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SchoolViewHolder, position: Int) {
        holder.bind(schoolList[position])
    }

    override fun getItemCount(): Int {
        return schoolList.size
    }

    fun setData(data: List<School>){
        schoolList = data
        notifyDataSetChanged()
    }

    inner class SchoolViewHolder(
        private val binding: ListItemBinding
    ): RecyclerView.ViewHolder(binding.root){

        fun bind(school: School){
            binding.apply {
                schoolName.text = school.school_name
                schoolInfo.text = school.phone_number
                schoolEmail.text = school.school_email

                root.setOnClickListener {
                    itemClicked(school)
                }
            }
        }
    }
}

