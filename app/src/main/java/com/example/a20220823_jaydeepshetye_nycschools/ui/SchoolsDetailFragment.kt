package com.example.a20220823_jaydeepshetye_nycschools.ui

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.example.a20220823_jaydeepshetye_nycschools.R
import com.example.a20220823_jaydeepshetye_nycschools.databinding.FragmentSchoolDetailsBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SchoolsDetailFragment: Fragment(R.layout.fragment_school_details) {

    private val args: SchoolsDetailFragmentArgs by navArgs()
    private lateinit var binding: FragmentSchoolDetailsBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentSchoolDetailsBinding.bind(view)

        binding.apply {
            languageClasses.text = getString(R.string.language_classes, args.school.language_classes ?: "NA")
            detailsSchoolInfo.text = getString(R.string.detailed_info, args.school.overview_paragraph ?: "NA")
        }
    }
}

