package com.example.a20220823_jaydeepshetye_nycschools.api

import com.example.a20220823_jaydeepshetye_nycschools.model.School
import retrofit2.Response
import retrofit2.http.GET

interface SchoolsAPI {

    @GET("resource/s3k6-pzi2.json")
    suspend fun getSchoolsData(): Response<List<School>>
}

