package com.example.a20220823_jaydeepshetye_nycschools

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class SchoolsApp: Application()