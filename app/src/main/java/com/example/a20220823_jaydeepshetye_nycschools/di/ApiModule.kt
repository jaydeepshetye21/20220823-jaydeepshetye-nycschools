package com.example.a20220823_jaydeepshetye_nycschools.di

import com.example.a20220823_jaydeepshetye_nycschools.api.SchoolsAPI
import com.example.a20220823_jaydeepshetye_nycschools.utils.Constants
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object ApiModule {

    @Singleton
    @Provides
    fun providesSchoolsService(): SchoolsAPI{
        return Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(SchoolsAPI::class.java)
    }
}

