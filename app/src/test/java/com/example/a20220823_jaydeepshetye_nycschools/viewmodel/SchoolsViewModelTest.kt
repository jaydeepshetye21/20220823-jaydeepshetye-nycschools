package com.example.a20220823_jaydeepshetye_nycschools.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.a20220823_jaydeepshetye_nycschools.MainCoroutineRule
import com.example.a20220823_jaydeepshetye_nycschools.api.SchoolsAPI
import com.example.a20220823_jaydeepshetye_nycschools.model.School
import com.example.a20220823_jaydeepshetye_nycschools.repository.SchoolsRepository
import com.example.a20220823_jaydeepshetye_nycschools.utils.ResponseState.Failure
import com.example.a20220823_jaydeepshetye_nycschools.utils.ResponseState.Success
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock

@Suppress("DEPRECATION")
@OptIn(ExperimentalCoroutinesApi::class)
class SchoolsViewModelTest {

    @get:Rule
    val mainCoroutineRule = MainCoroutineRule()

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    private val schoolsAPI = mock(SchoolsAPI::class.java)
    private val schoolsRepository = SchoolsRepository(schoolsAPI)
    private lateinit var subject: SchoolsViewModel

    @Before
    fun setUp() {
        subject = SchoolsViewModel(schoolsRepository)
    }

    @Test
    fun `test is success`() {
        runBlockingTest {
            val schoolsData = subject.javaClass.getDeclaredMethod("getSchoolsData").apply {
                isAccessible = true
            }
            `when`(schoolsRepository.getSchoolsList()).thenReturn(successData)

            schoolsData.invoke(subject)

            subject.schools.observeForever {
                it is Success
            }
        }
    }

    @Test
    fun `test is failure`() {
        runBlockingTest {
            val schoolsData = subject.javaClass.getDeclaredMethod("getSchoolsData").apply {
                isAccessible = true
            }
            `when`(schoolsRepository.getSchoolsList()).thenReturn(failedData)

            schoolsData.invoke(subject)

            subject.schools.observeForever {
                it is Failure
            }
        }
    }

    companion object {
        val successData = Success(
            listOf(
                School(
                    school_name = "Clinton School Writers & Artists, M.S. 260",
                    phone_number = "212-524-4360",
                    overview_paragraph = "Students who are prepared for college must have an education that encourages them to take risks as they produce and perform.",
                    language_classes = "Spanish",
                    school_email = "admissions@theclintonschool.net"
                ),
                School(
                    school_name = "Liberation Diploma Plus High School",
                    phone_number = "718-946-6812",
                    overview_paragraph = "The mission of Liberation Diploma Plus High School, in partnership with CAMBA, is to develop the student academically, socially, and emotionally.",
                    language_classes = "French, Spanish",
                    school_email = "scaraway@schools.nyc.gov"
                )
            )
        )
        val failedData = Failure(Exception())
    }
}

