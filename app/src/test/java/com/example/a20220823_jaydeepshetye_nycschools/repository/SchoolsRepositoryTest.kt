package com.example.a20220823_jaydeepshetye_nycschools.repository

import com.example.a20220823_jaydeepshetye_nycschools.api.SchoolsAPI
import com.example.a20220823_jaydeepshetye_nycschools.model.School
import com.example.a20220823_jaydeepshetye_nycschools.utils.ResponseState.Failure
import com.example.a20220823_jaydeepshetye_nycschools.utils.ResponseState.Success
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import okhttp3.ResponseBody
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import retrofit2.Response

@OptIn(ExperimentalCoroutinesApi::class)
@RunWith(JUnit4::class)
class SchoolsRepositoryTest {

    private val schoolsAPI = mock(SchoolsAPI::class.java)
    private lateinit var subject: SchoolsRepository

    @Before
    fun setUp() {
        subject = SchoolsRepository(schoolsAPI)
    }

    @Test
    fun `API Success`() {
        runBlockingTest {
            val response = Response.success(successData)
            `when`(schoolsAPI.getSchoolsData()).thenReturn(response)

            val responseState = subject.getSchoolsList()

            assert(responseState is Success)
        }
    }

    @Test
    fun `API Failure`() {
        runBlockingTest {
            val response = Response.error<List<School>>(400, ResponseBody.create(null, ""))
            `when`(schoolsAPI.getSchoolsData()).thenReturn(response)

            val responseState = subject.getSchoolsList()

            assert(responseState is Failure)
        }
    }

    companion object {
        val successData = listOf(
            School(
                school_name = "Clinton School Writers & Artists, M.S. 260",
                phone_number = "212-524-4360",
                overview_paragraph = "Students who are prepared for college must have an education that encourages them to take risks as they produce and perform.",
                language_classes = "Spanish",
                school_email = "admissions@theclintonschool.net"
            ),
            School(
                school_name = "Liberation Diploma Plus High School",
                phone_number = "718-946-6812",
                overview_paragraph = "The mission of Liberation Diploma Plus High School, in partnership with CAMBA, is to develop the student academically, socially, and emotionally.",
                language_classes = "French, Spanish",
                school_email = "scaraway@schools.nyc.gov"
            )
        )
    }
}

